package ru.pisarev.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.constant.TerminalConst;
import ru.pisarev.tm.exception.AbstractException;

public class UnknownListenerException extends AbstractException {

    @NotNull
    public UnknownListenerException(@NotNull String command) {
        super("Incorrect command '" + command + "'. Use " + TerminalConst.CMD_HELP + " for display list of terminal listeners.");
    }

}
