package ru.pisarev.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.pisarev.tm.api.repository.dto.IProjectRecordRepository;
import ru.pisarev.tm.api.repository.dto.ITaskRecordRepository;
import ru.pisarev.tm.api.service.dto.IProjectTaskRecordService;
import ru.pisarev.tm.dto.TaskRecord;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyIndexException;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.exception.entity.ProjectNotFoundException;
import ru.pisarev.tm.repository.dto.ProjectRecordRepository;
import ru.pisarev.tm.repository.dto.TaskRecordRepository;
import ru.pisarev.tm.service.AbstractService;

import java.util.List;
import java.util.Optional;

@Service
public final class ProjectTaskRecordService extends AbstractService implements IProjectTaskRecordService {

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskRecord> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            return repository.findAllTaskByProjectId(userId, projectId);
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void bindTaskById(
            @NotNull final String userId, @Nullable final String taskId, @Nullable final String projectId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            repository.bindTaskToProjectById(userId, taskId, projectId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskById(@NotNull final String userId, @Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        try {
            repository.getTransaction().begin();

            repository.unbindTaskById(userId, taskId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        @NotNull final IProjectRecordRepository projectRepository = context.getBean(ProjectRecordRepository.class);
        try {
            repository.getTransaction().begin();
            repository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByIdUserId(userId, projectId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }


    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        @NotNull final IProjectRecordRepository projectRepository = context.getBean(ProjectRecordRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final String projectId = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();
            repository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByIdUserId(userId, projectId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRecordRepository repository = context.getBean(TaskRecordRepository.class);
        @NotNull final IProjectRecordRepository projectRepository = context.getBean(ProjectRecordRepository.class);
        try {
            repository.getTransaction().begin();

            @NotNull final String projectId = Optional.ofNullable(projectRepository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();
            repository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByIdUserId(userId, projectId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

}