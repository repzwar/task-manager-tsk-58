package ru.pisarev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.listener.AbstractListener;
import ru.pisarev.tm.service.ListenerService;

@Component
public class DisplayListener extends AbstractListener {

    @NotNull
    @Autowired
    private ListenerService listenerService;

    @Override
    public String name() {
        return "listeners";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show program listeners.";
    }

    @Override
    @EventListener(condition = "@displayListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        int index = 1;
        for (@NotNull final String listener : listenerService.getListListenerName()) {
            System.out.println(index + ". " + listener);
            index++;
        }
    }

}
