package ru.pisarev.tm.api.repository.dto;

import ru.pisarev.tm.api.IRecordRepository;
import ru.pisarev.tm.dto.TaskRecord;

import java.util.List;

public interface ITaskRecordRepository extends IRecordRepository<TaskRecord> {

    List<TaskRecord> findAllTaskByProjectId(final String userId, final String projectId);

    void removeAllTaskByProjectId(final String userId, final String projectId);

    void bindTaskToProjectById(final String userId, final String taskId, final String projectId);

    void unbindTaskById(final String userId, final String id);

    void update(final TaskRecord task);

    TaskRecord findByIdUserId(final String userId, final String id);

    void clearByUserId(final String userId);

    void removeByIdUserId(final String userId, final String id);

    List<TaskRecord> findAllByUserId(final String userId);

    TaskRecord findByName(final String userId, final String name);

    TaskRecord findByIndex(final String userId, final int index);

    void removeByName(final String userId, final String name);

    void removeByIndex(final String userId, final int index);

}
