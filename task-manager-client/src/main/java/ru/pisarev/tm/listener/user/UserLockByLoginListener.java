package ru.pisarev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.endpoint.AdminEndpoint;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.listener.AbstractListener;
import ru.pisarev.tm.util.TerminalUtil;

@Component
public class UserLockByLoginListener extends AbstractListener {

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Lock user by login";
    }

    @Override
    @EventListener(condition = "@userLockByLoginListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter login");
        @Nullable final String login = TerminalUtil.nextLine();
        adminEndpoint.lockByLogin(getSession(), login);
    }

}
