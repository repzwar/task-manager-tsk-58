package ru.pisarev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.endpoint.SessionDto;

public interface SessionLocator {

    @Nullable
    SessionDto getSession();

    void setSession(SessionDto session);
}
