package ru.pisarev.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pisarev.tm.api.IPropertyService;
import ru.pisarev.tm.api.repository.dto.ISessionRecordRepository;
import ru.pisarev.tm.api.service.dto.ISessionRecordService;
import ru.pisarev.tm.api.service.dto.IUserRecordService;
import ru.pisarev.tm.dto.SessionRecord;
import ru.pisarev.tm.dto.UserRecord;
import ru.pisarev.tm.enumerated.Role;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.system.AccessDeniedException;
import ru.pisarev.tm.repository.dto.SessionRecordRepository;
import ru.pisarev.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public final class SessionRecordService extends AbstractRecordService<SessionRecord> implements ISessionRecordService {

    @NotNull
    @Autowired
    private IUserRecordService userService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionRecord> findAll() {
        @NotNull final ISessionRecordRepository repository = context.getBean(SessionRecordRepository.class);
        try {
            return repository.findAll();
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<SessionRecord> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (SessionRecord item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionRecord add(@Nullable final SessionRecord entity) {
        if (entity == null) return null;
        @NotNull final ISessionRecordRepository repository = context.getBean(SessionRecordRepository.class);
        try {
            repository.getTransaction().begin();

            repository.add(entity);
            repository.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionRecord findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final ISessionRecordRepository repository = context.getBean(SessionRecordRepository.class);
        try {
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final ISessionRecordRepository repository = context.getBean(SessionRecordRepository.class);
        try {
            repository.getTransaction().begin();

            repository.clear();
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final ISessionRecordRepository repository = context.getBean(SessionRecordRepository.class);
        try {
            repository.getTransaction().begin();

            repository.removeById(optionalId.orElseThrow(EmptyIdException::new));
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final SessionRecord entity) {
        if (entity == null) return;
        @NotNull final ISessionRecordRepository repository = context.getBean(SessionRecordRepository.class);
        try {
            repository.getTransaction().begin();

            repository.removeById(entity.getId());
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    @NotNull
    public SessionRecord open(@Nullable final String login, @Nullable final String password) {
        @Nullable final UserRecord user = checkDataAccess(login, password);
        if (user == null) throw new AccessDeniedException();
        final SessionRecord session = new SessionRecord();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        @Nullable final SessionRecord resultSession = sign(session);
        add(resultSession);
        return resultSession;
    }

    @Override
    @SneakyThrows
    public UserRecord checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable final UserRecord user = userService.findByLogin(login);
        if (user == null) return null;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null || hash.isEmpty() || user.isLocked()) return null;
        if (hash.equals(user.getPasswordHash())) {
            return user;
        } else {
            return null;
        }
    }

    @Override
    @SneakyThrows
    public void validate(@NotNull final SessionRecord session, final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserRecord user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionRecord session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionRecord temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final ISessionRecordRepository repository = context.getBean(SessionRecordRepository.class);
        try {
            if (repository.findById(session.getId()) == null) throw new AccessDeniedException();
        } finally {
            repository.close();
        }

    }

    @Override
    @SneakyThrows
    @Nullable
    public SessionRecord sign(@Nullable final SessionRecord session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.sign(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    @SneakyThrows
    public void close(@Nullable final SessionRecord session) {
        @NotNull final ISessionRecordRepository repository = context.getBean(SessionRecordRepository.class);
        try {
            repository.getTransaction().begin();

            repository.removeById(session.getId());
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void closeAllByUserId(@Nullable final String userId) {
        if (userId == null) return;
        @NotNull final ISessionRecordRepository repository = context.getBean(SessionRecordRepository.class);
        try {
            repository.getTransaction().begin();

            repository.removeByUserId(userId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    @Nullable
    public List<SessionRecord> findAllByUserId(@Nullable final String userId) {
        if (userId == null) return null;
        @NotNull final ISessionRecordRepository repository = context.getBean(SessionRecordRepository.class);
        try {
            return repository.findAllByUserId(userId);
        } finally {
            repository.close();
        }
    }
}
