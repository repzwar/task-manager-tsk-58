package ru.pisarev.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.pisarev.tm.api.repository.model.ITaskRepository;
import ru.pisarev.tm.model.TaskGraph;

import java.util.List;

@Repository
@Scope("prototype")
public final class TaskGraphRepository extends AbstractGraphRepository<TaskGraph> implements ITaskRepository {

    public TaskGraph findById(@Nullable final String id) {
        return entityManager.find(TaskGraph.class, id);
    }

    public TaskGraph getReference(@NotNull final String id) {
        return entityManager.getReference(TaskGraph.class, id);
    }

    public void removeById(@Nullable final String id) {
        TaskGraph reference = entityManager.getReference(TaskGraph.class, id);
        entityManager.remove(reference);
    }

    @NotNull
    public List<TaskGraph> findAll() {
        return entityManager.createQuery("SELECT e FROM TaskGraph e", TaskGraph.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public List<TaskGraph> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM TaskGraph e WHERE e.user.id = :userId", TaskGraph.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public List<TaskGraph> findAllTaskByProjectId(String userId, String projectId) {
        return entityManager
                .createQuery(
                        "SELECT e FROM TaskGraph e WHERE e.user.id = :userId AND e.project.id = :projectId",
                        TaskGraph.class
                )
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeAllTaskByProjectId(String userId, String projectId) {
        entityManager
                .createQuery("DELETE FROM TaskGraph e WHERE e.user.id = :userId AND e.project.id = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void bindTaskToProjectById(String userId, String taskId, String projectId) {
        entityManager
                .createQuery("UPDATE Task e SET e.project.id = :projectId WHERE e.user.id AND e.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void unbindTaskById(String userId, String id) {
        entityManager
                .createQuery("UPDATE Task e SET e.project.id = NULL WHERE e.user.id AND e.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public TaskGraph findByIdUserId(String userId, String id) {
        return getSingleResult(
                entityManager
                        .createQuery(
                                "SELECT e FROM TaskGraph e WHERE e.id = :id AND e.user.id = :userId",
                                TaskGraph.class
                        )
                        .setHint(QueryHints.HINT_CACHEABLE, true)
                        .setParameter("id", id)
                        .setParameter("userId", userId)
                        .setMaxResults(1)
        );
    }


    @Nullable
    @Override
    public TaskGraph findByName(@NotNull final String userId, @Nullable final String name) {
        return getSingleResult(
                entityManager
                        .createQuery(
                                "SELECT e FROM TaskGraph e WHERE e.name = :name AND e.user.id = :userId",
                                TaskGraph.class
                        )
                        .setParameter("name", name)
                        .setParameter("userId", userId)
                        .setMaxResults(1)
        );
    }

    @Nullable
    @Override
    public TaskGraph findByIndex(@NotNull final String userId, final int index) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM TaskGraph e WHERE e.user.id = :userId", TaskGraph.class)
                        .setParameter("userId", userId)
                        .setFirstResult(index)
                        .setMaxResults(1)
        );
    }

    @Override
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM TaskGraph e WHERE e.name = :name AND e.user.id = :userId")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM TaskGraph e WHERE e.user.id = :userId")
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM TaskGraph e")
                .executeUpdate();
    }

    @Override
    public void clearByUserId(String userId) {
        entityManager
                .createQuery("DELETE FROM TaskGraph e WHERE e.user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeByIdUserId(String userId, String id) {
        entityManager
                .createQuery("DELETE FROM TaskGraph e WHERE e.user.id = :userId AND e.id = :id")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}