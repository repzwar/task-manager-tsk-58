package ru.pisarev.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.endpoint.TaskDto;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.exception.entity.TaskNotFoundException;


public abstract class TaskAbstractListener extends AbstractListener {

    protected void show(@Nullable final TaskDto task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + Status.valueOf(task.getStatus().value()).getDisplayName());
        System.out.println("ProjectDto Id: " + task.getProjectId());
    }

    @NotNull
    protected TaskDto add(@Nullable final String name, @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskDto task = new TaskDto();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Nullable
    protected String toString(TaskDto task) {
        return task.getId() + ": " + task.getName();
    }

}
