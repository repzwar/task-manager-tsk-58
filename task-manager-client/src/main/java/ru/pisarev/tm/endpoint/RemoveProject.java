package ru.pisarev.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for removeProject complex type.
 * <p>
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * <p>
 * &lt;pre&gt;
 * &amp;lt;complexType name="removeProject"&amp;gt;
 * &amp;lt;complexContent&amp;gt;
 * &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 * &amp;lt;sequence&amp;gt;
 * &amp;lt;element name="session" type="{http://endpoint.tm.pisarev.ru/}sessionDto" minOccurs="0"/&amp;gt;
 * &amp;lt;element name="entity" type="{http://endpoint.tm.pisarev.ru/}projectDto" minOccurs="0"/&amp;gt;
 * &amp;lt;/sequence&amp;gt;
 * &amp;lt;/restriction&amp;gt;
 * &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "removeProject", propOrder = {
        "session",
        "entity"
})
public class RemoveProject {

    protected SessionDto session;
    protected ProjectDto entity;

    /**
     * Gets the value of the session property.
     *
     * @return possible object is
     * {@link SessionDto }
     */
    public SessionDto getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     *
     * @param value allowed object is
     *              {@link SessionDto }
     */
    public void setSession(SessionDto value) {
        this.session = value;
    }

    /**
     * Gets the value of the entity property.
     *
     * @return possible object is
     * {@link ProjectDto }
     */
    public ProjectDto getEntity() {
        return entity;
    }

    /**
     * Sets the value of the entity property.
     *
     * @param value allowed object is
     *              {@link ProjectDto }
     */
    public void setEntity(ProjectDto value) {
        this.entity = value;
    }

}
