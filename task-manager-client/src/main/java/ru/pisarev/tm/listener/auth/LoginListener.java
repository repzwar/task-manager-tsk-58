package ru.pisarev.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.endpoint.SessionDto;
import ru.pisarev.tm.endpoint.SessionEndpoint;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.listener.AuthAbstractListener;
import ru.pisarev.tm.util.TerminalUtil;

@Component
public class LoginListener extends AuthAbstractListener {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Override
    public String name() {
        return "login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logging in to application.";
    }

    @Override
    @EventListener(condition = "@loginListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter login");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        @Nullable final String password = TerminalUtil.nextLine();
        SessionDto session = sessionEndpoint.open(login, password);
        sessionLocator.setSession(session);
    }

}
