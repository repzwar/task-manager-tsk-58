package ru.pisarev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.endpoint.TaskDto;
import ru.pisarev.tm.endpoint.TaskEndpoint;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.listener.TaskAbstractListener;

import java.util.List;

@Component
public class TaskShowListListener extends TaskAbstractListener {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    @EventListener(condition = "@taskShowListListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable List<TaskDto> tasks = taskEndpoint.findTaskAll(getSession());
        int index = 1;
        for (@NotNull TaskDto task : tasks) {
            System.out.println(index + ". " + toString(task));
            index++;
        }
    }

}
