package ru.pisarev.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.pisarev.tm.endpoint.SessionDto;

@Getter
@Setter
@Service
public class SessionService {

    @Nullable
    private SessionDto session;

}
