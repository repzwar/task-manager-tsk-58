package ru.pisarev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pisarev.tm.api.IPropertyService;
import ru.pisarev.tm.api.repository.model.IUserRepository;
import ru.pisarev.tm.api.service.model.IUserService;
import ru.pisarev.tm.exception.empty.*;
import ru.pisarev.tm.exception.entity.EmailExistException;
import ru.pisarev.tm.exception.entity.LoginExistException;
import ru.pisarev.tm.exception.entity.UserNotFoundException;
import ru.pisarev.tm.model.UserGraph;
import ru.pisarev.tm.repository.model.UserGraphRepository;
import ru.pisarev.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public final class UserGraphService extends AbstractGraphService<UserGraph> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    @SneakyThrows
    public List<UserGraph> findAll() {
        @NotNull final IUserRepository repository = context.getBean(UserGraphRepository.class);
        try {
            return repository.findAll();
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<UserGraph> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (UserGraph item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserGraph add(@Nullable final UserGraph entity) {
        if (entity == null) return null;
        @NotNull final IUserRepository repository = context.getBean(UserGraphRepository.class);
        try {
            repository.getTransaction().begin();
            repository.add(entity);
            repository.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserGraph findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final IUserRepository repository = context.getBean(UserGraphRepository.class);
        try {
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final IUserRepository repository = context.getBean(UserGraphRepository.class);
        try {
            repository.clear();
            @NotNull final List<UserGraph> users = repository.findAll();
            for (UserGraph t :
                    users) {
                repository.remove(t);
            }
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final IUserRepository repository = context.getBean(UserGraphRepository.class);
        try {
            @Nullable final UserGraph user = repository
                    .getReference(optionalId.orElseThrow(EmptyIdException::new));
            if (user == null) return;
            repository.remove(user);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final UserGraph entity) {
        if (entity == null) return;
        @NotNull final IUserRepository repository = context.getBean(UserGraphRepository.class);
        try {
            @Nullable final UserGraph user = repository.getReference(entity.getId());
            if (user == null) return;
            repository.remove(user);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserGraph findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserRepository repository = context.getBean(UserGraphRepository.class);
        try {
            return repository.findByLogin(login);
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public UserGraph findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final IUserRepository repository = context.getBean(UserGraphRepository.class);
        try {
            return repository.findByEmail(email);
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserRepository repository = context.getBean(UserGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @Nullable final UserGraph user = repository.findByLogin(login);
            if (user == null) return;
            repository.remove(user);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph add(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExist(login)) throw new LoginExistException(login);
        @NotNull final UserGraph user = new UserGraph();
        user.setLogin(login);
        @NotNull final String passwordHash = Optional.ofNullable(HashUtil.salt(propertyService, password))
                .orElseThrow(EmptyPasswordHashException::new);
        user.setPasswordHash(passwordHash);
        add(user);

        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph add(
            @Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExist(login)) throw new LoginExistException(login);
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isEmailExist(email)) throw new EmailExistException(login);
        @NotNull final UserGraph user = new UserGraph();
        user.setLogin(login);
        user.setEmail(email);
        @NotNull final String passwordHash = Optional.ofNullable(HashUtil.salt(propertyService, password))
                .orElseThrow(EmptyPasswordHashException::new);
        user.setPasswordHash(passwordHash);
        @NotNull final IUserRepository repository = context.getBean(UserGraphRepository.class);
        try {
            repository.getTransaction().begin();
            add(user);
            repository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final UserGraph user = findById(id);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String passwordHash = Optional.ofNullable(HashUtil.salt(propertyService, password))
                .orElseThrow(EmptyPasswordHashException::new);
        user.setPasswordHash(passwordHash);
        @NotNull final IUserRepository repository = context.getBean(UserGraphRepository.class);
        try {
            repository.update(user);
            repository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }


    @Override
    @SneakyThrows
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final UserGraph user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final IUserRepository repository = context.getBean(UserGraphRepository.class);
        try {
            repository.update(user);
            repository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph lockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserGraph user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final IUserRepository repository = context.getBean(UserGraphRepository.class);
        try {
            repository.update(user);
            repository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph unlockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserGraph user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final IUserRepository repository = context.getBean(UserGraphRepository.class);
        try {
            repository.update(user);
            repository.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

}
