package ru.pisarev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IRepository;
import ru.pisarev.tm.model.AbstractGraph;
import ru.pisarev.tm.repository.AbstractRepository;

import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.List;

public abstract class AbstractGraphRepository<E extends AbstractGraph> extends AbstractRepository implements IRepository<E> {


    public void add(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.persist(entity);
    }

    public void update(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.merge(entity);
    }

    public void addAll(@Nullable final Collection<E> collection) {
        if (collection == null) return;
        for (E item : collection) {
            add(item);
        }
    }

    public void remove(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.remove(entity);
    }

    protected E getSingleResult(@NotNull final TypedQuery<E> query) {
        @NotNull final List<E> list = query.getResultList();
        if (list.isEmpty()) return null;
        return list.get(0);
    }

}