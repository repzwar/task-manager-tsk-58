package ru.pisarev.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.endpoint.SessionEndpoint;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.listener.AuthAbstractListener;
import ru.pisarev.tm.util.TerminalUtil;

@Component
public class RegistryListener extends AuthAbstractListener {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Override
    public String name() {
        return "registry";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create new user";
    }

    @Override
    @EventListener(condition = "@registryListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter login");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("Enter email");
        @Nullable final String email = TerminalUtil.nextLine();
        sessionEndpoint.register(login, password, email);
    }

}
